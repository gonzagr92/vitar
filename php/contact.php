<?php
if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){
    //your site secret key
    $secret = '6LdBolwUAAAAAK7ItpCFZr6LrDcW4CgpR_Gvrhyo';
    //get verify response data
    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
    $responseData = json_decode($verifyResponse);
    if($responseData->success){

        ob_start();
        include 'contact_mailer.php';
        $msg = ob_get_contents();
        ob_end_clean();

        $to  = 'gonza.gr92@gmail.com';
        $subject = $_POST['subject'];

        $headers = 'MIME-Version: 1.0'."\r\n";
        $headers .= 'Content-type:text/html;charset=UTF-8'."\r\n";
        $headers .= 'From: Vitar Web <web@vitarturismo.com>' . "\r\n";

        // Enviarlo
        try {
            mail($to, $subject, $msg, $headers);
            json_encode(array('info' => 'success', 'msg' => 'Mensaje enviado'));
        } catch (Exception $exc) {
            json_encode(array('info' => 'error', 'msg' => 'Se produjo un error'));
        }
    }else{
        json_encode(array('info' => 'error', 'msg' => 'Robot verification failed, please try again.'));
    }
}else{
    json_encode(array('info' => 'error', 'msg' => 'Please click on the reCAPTCHA box.'));
}
?>
